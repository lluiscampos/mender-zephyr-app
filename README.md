# mender-zephyr-app

A sample project on how to use Mender Zephyr module.

This is a PoC to learn how a sample application will use Mender as a Zephyr OS module.

The important bits are:
* The manifest file `west.yml` with the dependencies: Zephyr OS and Mender module
* The project configuration `prj.conf` enabling Mender module
* The demo source code and CMake code to build the app

## Getting started

Install Zephyr requirements:
```
pip install west pyelftools
```

Create the Zephyr workspace
```
west init workspace --manifest-url https://gitlab.com/lluiscampos/mender-zephyr-app
cd workspace && west update
```

### QEMU

Compile, run and cross fingers
```
west build --build-dir build-qemu --board qemu_x86 mender-zephyr-app
west build --build-dir build-qemu --target run
```

### The real deal - Nordic Semi eval board

https://docs.zephyrproject.org/latest/boards/nordic/nrf52840dk/doc/index.html#flashing

Open a the serial:
```
minicom -D /dev/ttyACM1
```

And build, flash, blink:
```
west build --build-dir build-nordic --board nrf52840dk/nrf52840 mender-zephyr-app
west flash --build-dir build-nordic
```

### Building natively

```
west build --build-dir build-native --board native_sim mender-zephyr-app
./build-native/zephyr/zephyr.exe
```
