#include <vector>
#include <string>

#include <stdio.h>

#include <common/key_value_parser.hpp>

namespace kvp = mender::common::key_value_parser;

using namespace std;

int main(void)
{
	printf("Hello World! %s\n", CONFIG_BOARD_TARGET);

	vector<string> items = {"hello=mender", "it=works"};

	kvp::ExpectedKeyValueMap ret = kvp::ParseKeyValueMap(items);

	if (!ret) {
		printf("error: %s\n", ret.error().String().c_str());
	} else {
		for (auto pair : ret.value()) {
			printf("%s=%s\n", pair.first.c_str(), pair.second.c_str());
		}
	}

	printf("Bye World! %s\n", CONFIG_BOARD_TARGET);

	return 0;
}
